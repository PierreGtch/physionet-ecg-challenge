import keras
from keras.models import Model
from keras.layers.normalization import BatchNormalization
from keras.layers import (Dense, Dropout, TimeDistributed, Input, concatenate,
                          LSTM,  Masking, Lambda, Activation)
from TemporalMeanPooling import *
from TemporalMaxPooling import *

def create_attnn(n_time_steps, n_features, n_classes, params, pad_value, feature_position=None):
    inputs = Input(shape=(n_time_steps, n_features))

    long_RR = Lambda(lambda x: x[:, :, -5:-4], output_shape=(n_time_steps, 1))(inputs)
    short_RR = Lambda(lambda x: x[:, :, -4:-3], output_shape=(n_time_steps, 1))(inputs)
    long_QRSd = Lambda(lambda x: x[:, :, -3:-2], output_shape=(n_time_steps, 1))(inputs)
    long_PR = Lambda(lambda x: x[:, :, -2:-1], output_shape=(n_time_steps, 1))(inputs)
    pnn50 = Lambda(lambda x: x[:, :, -1:], output_shape=(n_time_steps, 1))(inputs)

    thresholds = concatenate([
        TimeDistributed(
            Dense(1, activation='tanh', kernel_initializer=keras.initializers.Ones(),
                  bias_initializer=keras.initializers.Zeros()
                  ))(long_RR),
        TimeDistributed(
            Dense(1, activation='tanh', kernel_initializer=keras.initializers.Ones(),
                  bias_initializer=keras.initializers.Zeros()
                  ))(short_RR),
        TimeDistributed(
            Dense(1, activation='tanh', kernel_initializer=keras.initializers.Ones(),
                  bias_initializer=keras.initializers.Zeros()
                  ))(long_QRSd),
        TimeDistributed(
            Dense(1, activation='tanh', kernel_initializer=keras.initializers.Ones(),
                  bias_initializer=keras.initializers.Zeros()
                  ))(long_PR),
        TimeDistributed(
            Dense(1, activation='tanh', kernel_initializer=keras.initializers.Ones(),
                  bias_initializer=keras.initializers.Zeros()
                  ))(pnn50)
    ])

    augmented_inputs = concatenate([inputs, thresholds])
    masked_inputs = Masking(mask_value=pad_value)(augmented_inputs)

    if params['n_mlp_in'] > 0:
        processed_inputs = TimeDistributed(
            Dense(params['n_mlp_in'])
        )(masked_inputs)
        # processed_inputs = TimeDistributed(
        #     BatchNormalization()
        # )(processed_inputs)
        processed_inputs = TimeDistributed(
            Activation('relu')
        )(processed_inputs)
        processed_inputs = TimeDistributed(
            Dropout(params['mlp_in_dropout'])
        )(processed_inputs)
        processed_inputs = TimeDistributed(
            Dense(params['n_lstm_units'])
        )(processed_inputs)
        # processed_inputs = TimeDistributed(BatchNormalization())(processed_inputs)
    else:
        processed_inputs = masked_inputs

    lstm1 = (
        LSTM(params['n_lstm_units'], return_sequences=True,
             dropout=params['lstm_dropout'],
             recurrent_dropout=params['lstm_recurrent_dropout'])(processed_inputs)
    )
    lstm2 = (
        LSTM(params['n_lstm_units'],
             dropout=params['lstm_dropout_2'],
             recurrent_dropout=params['lstm_recurrent_dropout_2'])
    )(lstm1)

    if True:
        lstm3 = (
            LSTM(params['n_lstm_units'], return_sequences=True,
                 dropout=params['lstm_dropout_2'],
                 recurrent_dropout=params['lstm_recurrent_dropout_2'])
        )(lstm1)
        lstm3 = TemporalMaxPooling()(lstm3)

        lstm4 = (
            LSTM(params['n_lstm_units'], return_sequences=True,
                 dropout=params['lstm_dropout_2'],
                 recurrent_dropout=params['lstm_recurrent_dropout_2'])
        )(lstm1)
        lstm4 = TemporalMeanPooling()(lstm4)

        lstm_out = concatenate([lstm2, lstm3, lstm4])
    else:
        lstm_out = lstm2

    if params['n_mlp_out_hidden'] > 0:
        lstm_out = Dense(params['n_mlp_out_hidden'])(lstm_out)
        lstm_out = BatchNormalization()(lstm_out)
        lstm_out = Activation('relu')(lstm_out)
        lstm_out = Dropout(params['mlp_out_dropout'])(lstm_out)
        lstm_out = Dense(params['n_mlp_out_out'])(lstm_out)

    lstm_out = BatchNormalization()(lstm_out)
    output = Dense(n_classes, activation='softmax')(lstm_out)
    return Model(inputs, output)
