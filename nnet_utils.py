import numpy as np
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, LSTM, Masking

# PREPROCESSING PARAMETERS
NNET_PREPROC_VARS = {
    'na_value': np.nan,
    'pad_value': 0.0,
    'max_sequence_len': 150,
    'ecg_center': 0.0,
    'ecg_scale': 256.0,
    'n_morhp': 28,
    'n_rh' : 12,
    'oh_feat': [0, 1],
    'n_features': 37,
    'n_encoded_features': 75 + 5 + 5 - 28,
    'n_encoded_categorical': 45 - 28 + 5,
    'ecg_len' : 255, # len of raw ECG use for the CNN
    'categorical_feat': np.arange(7),
    'RR': 8,
    'RRda': 10,
    'QRSd': 21,
    'PR': 24
}

## NEURAL NETS PARAMETERS
ATTNN_PARAMS = {
    'n_mlp_in': 256,
    'n_mlp_out_hidden': 256,
    'n_mlp_out_out': 128,
    'n_lstm_units': 128,
    'lstm_dropout': 0.21727770525137205,
    'lstm_dropout_1': 0.21727770525137205,
    'lstm_dropout_2': 0.3571560816885358,
    'lstm_dropout_3': 0.3571560816885358,
    'lstm_dropout_4': 0.3571560816885358,
    'lstm_recurrent_dropout': 0.44056818253743985,
    'lstm_recurrent_dropout_1': 0.44056818253743985,
    'lstm_recurrent_dropout_2': 0.36173203929367154,
    'lstm_recurrent_dropout_3': 0.36173203929367154,
    'lstm_recurrent_dropout_4': 0.36173203929367154 ,

    'mlp_out_dropout': 0.25,
    'mlp_in_dropout': 0.25
}

def transform_pad_and_reshape(features_list, transformer, preproc_vars):
    features_out = []
    for feat in features_list:
        if len(feat) == 0:
            transformed_feat = np.full((1, preproc_vars['n_encoded_features']), preproc_vars['pad_value'])
        else:
            transformed_feat = transformer.transform(feat)

        features_out.append(
            pad_construe_sequences(transformed_feat, preproc_vars)
        )
    return np.stack(features_out)


def pad_construe_sequences(features, preproc_vars):
    return np.transpose(
        sequence.pad_sequences(np.transpose(np.array(features)),
                               maxlen=preproc_vars['max_sequence_len'],
                               dtype='float32', padding='pre',
                               truncating='pre', value=preproc_vars['pad_value'])
    )
