#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 17:43:05 2017

Utility functions for the feature extraction task.

@author: tomas.teijeiro
"""

import numpy as np
import itertools as it
from statsmodels.nonparametric.kde import kdensityfft
from construe.utils.units_helper import (msec2samples as ms2sp,
                                         samples2msec as sp2ms)
from construe.utils.signal_processing.xcorr_similarity import xcorr_valid

def get_axis(beat):
    """
    Obtains the heart axis of a QRS complex, using only the information in
    the  MLII lead. The axis is therefore calculated only wrt this lead, and
    the value will be in the range [-90º,90º], being 90º the value obtained
    when all the amplitude is positive in lead MLII. In common heart axis scale,
    the range is [-120º,60º]. If no shapeform is recognized in lead MLII, the
    axis is undetermined (None).
    """
    shape = next(iter(beat.shape.values()))
    poswav = [w.amp for w in shape.waves if w.sign > 0]
    negwav = [w.amp for w in shape.waves if w.sign < 0]
    pamp = max(poswav) if poswav else 0.0
    namp = -max(negwav) if negwav else 0.0
    angle = int(90.0*(pamp-namp)/float(shape.amplitude))
    return angle

def discretize_axis(axis):
    if axis > 45:
        return 2
    elif axis < -45:
        return 0
    else:
        return 1

def profile(x):
    return np.sum(np.abs(np.diff(x)))

def get_interval_profile(sig, interv):
    wlen = int(ms2sp(500))
    prof = np.array([profile(sig[j:j+wlen])
                    for j in range(int(interv.start), int(interv.end), wlen)])
    return np.percentile(prof, 10)

def contiguous_regions(condition):
    """Finds contiguous True regions of the boolean array "condition". Returns
    a 2D array where the first column is the start index of the region and the
    second column is the end index."""
    d = np.diff(condition)
    idx, = d.nonzero()
    idx += 1
    if condition[0]:
        idx = np.r_[0, idx]
    if condition[-1]:
        idx = np.r_[idx, condition.size]
    idx.shape = (-1,2)
    return idx

def shape_xcorr(beats, sig, percentile=80):
    """
    Obtains a measure of the cross-correlation of a signal in the areas
    delimited by a sequence of observations.
    """
    return np.percentile(np.array([xcorr_valid(sig[int(q1.earlystart):int(q1.lateend+1)],
                                               sig[int(q2.earlystart):int(q2.lateend+1)],
                                               True)[0]
                       for (q1, q2) in it.combinations(beats, 2)]), percentile)

def md_mad(arr):
    """Obtains the median and the median absolute deviation of a sequence"""
    arr = np.array(arr)
    m = np.median(arr)
    return np.array([m, np.median(np.abs(arr-m))])

def kde_mode(data):
    """
    Obtains an estimation of the mode of a distribution as the peak of the KDE
    """
    if len(data)==1:
        return data[0]
    data = np.array(data,dtype=np.double)
    density, grid, _ = kdensityfft(data)
    return grid[np.argmax(density)]

def morphology_entropy(beats):
    if len(beats) == 0:
        return 0.0
    X = np.array([[sp2ms(q.lateend-q.earlystart),
                           q.shape.values()[0].amplitude,
                           get_axis(q)] for q in beats])
    durbins = [0, 50, 80, 120, 160, 500]
    ampbins = np.arange(0, 6000, 300)
    axbins = [-90, -45, 45, 90]
    hst, _ = np.histogramdd(X, np.array([durbins, ampbins, axbins]))
    hst = hst/float(len(beats))
    nz = hst[hst!=0]
    return -np.sum(nz*np.log(nz))

def atrial_entropy(tpsig, fs=300.0):
    """
    Obtains an entropy measure of the ventricular activity frequencies.
    """
    fourier = ((np.absolute(np.fft.fft(tpsig, n=2048)))/2048.)**2
    freq = np.fft.fftfreq(2048, 1/fs)
    idx = np.where(np.logical_and(freq > 3, freq < 12))
    return spectral_entropy(freq[idx], fourier[idx])

def spectral_entropy(f, spec):
    # To read spec as a density function we need to calculate the normalization
    # constant Z
    Z = 0.0
    for i in range(len(spec) - 1):
        Z += (f[i + 1] - f[i]) * (spec[i] + spec[i + 1]) / 2.0
    if Z == 0.0:
        return 0.0
    pr = spec / Z
    # Entropy calculation as an expectation
    entropy = np.zeros_like(pr)
    nz_indices = np.where(pr != 0.0)
    entropy[nz_indices] = -pr[nz_indices] * np.log2(pr[nz_indices])
    integral = 0.0
    for i in range(len(entropy) - 1):
        integral += (f[i + 1] - f[i]) * (entropy[i] + entropy[i + 1]) / 2.0
    return integral
