#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 09:51:09 2017

Generates a set of .csv files to

@author: tomas.teijeiro
"""

import construe.utils.MIT as MIT
import construe.utils.MIT.interp2annots as interp2annots
import construe.knowledge.observables as o
from construe.model.observable import EventObservable
from construe.model.interval import Interval as Iv
from construe.utils.units_helper import (samples2msec as sp2ms,
                                         msec2samples as ms2sp)
from construe.utils.signal_processing.prevailing_frequency import (
                                                           get_prevailing_freq)
from construe.knowledge.abstraction_patterns.segmentation.QRS import QRS_SHAPES
from construe.knowledge.abstraction_patterns.segmentation.pwave import (
                                                                pwave_distance)
from feat_utils import get_axis, profile
import csv
import bisect
import difflib
import numpy as np
import pandas as pd
from collections import OrderedDict as OD

FEAT = ['Rh', 'Morph', 'w1detected', 'w2detected', 'Pwdetected', 'Twdetected',
        'TPdetected', 'Rpk', 'RR', 'RRdb', 'RRda', 'w0a', 'w0d', 'w0p', 'w1a',
        'w1d', 'w1p', 'w2a', 'w2d', 'w2p', 'Axis', 'QRSd', 'QRSa', 'pw_prof',
        'PR', 'Pwd', 'Pwa', 'Pwdist', 'Twd', 'Twa', 'QT', 'STdev', 'TPa', 'TPf',
        'TPdur', 'profile', 'baseline', 'signal']
QSHAPES = sorted(QRS_SHAPES)

def get_tag(rh):
    if isinstance(rh, o.Sinus_Rhythm):
        return 0.0
    elif isinstance(rh, o.Bradycardia):
        return 1.0
    elif isinstance(rh, o.Tachycardia):
        return 2.0
    elif isinstance(rh, o.Extrasystole):
        return 3.0
    elif isinstance(rh, o.Couplet):
        return 4.0
    elif isinstance(rh, o.Bigeminy):
        return 5.0
    elif isinstance(rh, o.Trigeminy):
        return 6.0
    elif isinstance(rh, o.RhythmBlock):
        return 7.0
    elif isinstance(rh, o.Asystole):
        return 8.0
    elif isinstance(rh, o.Ventricular_Flutter):
        return 9.0
    elif isinstance(rh, o.Atrial_Fibrillation):
        return 10.0
    raise ValueError('Unknown rhythm {0}'.format(rh))

def get_rnn_features(interp, sig):
    """
    Obtains a feature matrix to feed the LSTM network in order to perform
    sequence analysis.

    Parameters
    ----------
    interp:
        Interpretation of the record.
    sig:
        Raw signal. Lead inversion is assumed to be corrected.

    Returns:
    --------
    out:
        Pandas Dataframe with one row for each beat.
    """
    df = pd.DataFrame(columns=FEAT)
    dummy = EventObservable()
    getobs = interp.get_observations
    rhythms = list(getobs(o.Cardiac_Rhythm))
    pqt = list(getobs((o.PWave, o.QRS, o.TWave)))
    rows = []
    for i in range(len(pqt)):
        if (isinstance(pqt[i], o.QRS) and pqt[i].shape
                                                and pqt[i].lateend < len(sig)):
            #Feature vector for each QRS complex
            ift = {ft:np.nan for ft in FEAT}
            ift['w1detected'] = 0
            ift['w2detected'] = 0
            ift['Pwdetected'] = 0
            ift['Twdetected'] = 0
            ift['TPdetected'] = 0
            qrs = pqt[i]
            #Check if P and T waves are present
            j = next((j for j in range(i-1, -1, -1)
                                if isinstance(pqt[j], (o.PWave, o.QRS))), None)
            pw = (pqt[j] if j is not None and isinstance(pqt[j], o.PWave)
                                                                     else None)
            j = next((j for j in range(i+1, len(pqt))
                                if isinstance(pqt[j], (o.TWave, o.QRS))), None)
            tw = (pqt[j] if j is not None and isinstance(pqt[j], o.TWave)
                                                                     else None)
            #Interpreting rhythm
            dummy.time.set(qrs.time.start, qrs.time.start)
            idx = bisect.bisect(rhythms, dummy)-1
            if idx < 0:
                idx += 1
            if idx == len(rhythms):
                idx -= 1
            if idx >= 0:
                rh = rhythms[idx]
                if (isinstance(rh, (o.Extrasystole, o.Couplet))
                                            and rh.earlyend == dummy.earlyend):
                    rh = (rhythms[idx+1] if idx < len(rhythms)-1
                          else rhythms[idx-1])
                ift['Rh'] = get_tag(rh)
            else:
                ift['Rh'] = 11.0
            qrs0 = next((pqt[j] for j in range(i-1, -1, -1)
                                       if isinstance(pqt[j], o.QRS)), None)
            #Beat time
            ift['Rpk'] = sp2ms(qrs.time.start)
            #RR interval, or time of the first beat
            ift['RR'] = (sp2ms(qrs.time.start-qrs0.time.start)
                            if qrs0 is not None else sp2ms(qrs.time.start))
            #QRS morphology, axis, duration and amplitude
            shape = next(iter(qrs.shape.values()))
            if shape.waves[0].dur == 0:
                shape.waves = shape.waves[1:]
            ift['w0a'] = shape.waves[0].amp
            ift['w0d'] = shape.waves[0].dur
            ift['w0p'] = shape.waves[0].m-shape.waves[0].l
            if len(shape.waves) > 1:
                ift['w1a'] = shape.waves[1].amp
                ift['w1d'] = shape.waves[1].dur
                ift['w1p'] = shape.waves[1].m-shape.waves[1].l
                ift['w1detected'] = 1
                if len(shape.waves) > 2:
                    ift['w2a'] = shape.waves[2].amp
                    ift['w2d'] = shape.waves[2].dur
                    ift['w2p'] = shape.waves[2].m-shape.waves[2].l
                    ift['w2detected'] = 1
            #QRS morphology, axis, duration and amplitude
            tag = shape.tag
            if tag not in QRS_SHAPES:
                if tag in ('ss', 'sS'):
                    tag = 'Qs'
                else:
                    newtag = difflib.get_close_matches(tag, QRS_SHAPES, 1)[0]
                    print('Unkown shape {0} changed by {1}'.format(tag, newtag))
                    tag = newtag
            ift['Morph'] = bisect.bisect_left(QSHAPES, tag)
            ift['Axis'] = get_axis(qrs)
            ift['QRSd'] = sp2ms(qrs.lateend-qrs.earlystart)
            ift['QRSa'] = shape.amplitude
            #Profile of the P wave area
            pwarea = int(ms2sp(250))
            qs = int(qrs.earlystart)
            ift['pw_prof'] = profile(sig[max(qs-pwarea,0):qs])
            if pw is not None:
                ift['Pwdetected'] = 1
                ift['PR'] = sp2ms(qrs.earlystart-pw.earlystart)
                ift['Pwd'] = sp2ms(pw.lateend-pw.earlystart)
                ift['Pwa'] = next(iter(pw.amplitude.values()))
                ift['Pwdist'] = pwave_distance(sig[int(pw.earlystart):
                                                   int(pw.lateend+1)], 'MLII')
            if tw is not None:
                ift['Twdetected'] = 1
                ift['Twd'] = sp2ms(tw.lateend-tw.earlystart)
                ift['Twa'] = next(iter(tw.amplitude.values()))
                qt = sp2ms(tw.lateend - qrs.earlystart)
                #Corrected QT interval
                rr_ref = min(max(ift['RR'], 333.0), 1500.0)
                ift['QT'] = (qt/1e3 + 0.154*(1.0-rr_ref/1e3))*1e3
            ift['STdev'] = sig[int(qrs.lateend)]-sig[int(qrs.earlystart)]
            j = next((j for j in range(i-1, -1, -1)
                            if isinstance(pqt[j], (o.TWave, o.QRS))), None)
            tw0 = (pqt[j] if j is not None and isinstance(pqt[j], o.TWave)
                                                                 else None)
            if tw0 is not None:
                if pqt[j+1].earlystart > tw0.earlyend:
                    tp = Iv(int(tw0.earlyend+1), int(pqt[j+1].earlystart))
                    if tp.length > ms2sp(100):
                        sfr = sig[tp.start:tp.end]
                        ift['TPdetected'] = 1
                        ift['TPa'] = np.ptp(sfr)
                        ift['TPf'] = get_prevailing_freq(sfr-np.mean(sfr),
                                                         300.0)[0]
                        ift['baseline'] = profile(sfr)/((sp2ms(len(sfr))
                                                       * abs(ift['QRSa'])/1e6))
                    if pw is not None:
                        ift['TPdur'] = sp2ms(tp.length)
            prof_area = int(ms2sp(1000))
            pbeg = max(0, int(qrs.time.start - prof_area))
            pend = min(len(sig), int(qrs.time.start + prof_area))
            ift['profile'] = profile(sig[pbeg:pend])
            #We include the signal of the cardiac cycle in a fixed window
            #of 850 ms (255 samples). QRS always starts at sample 90
            bsig = np.zeros(255, dtype=int)
            sstart, bstart = qrs.earlystart, 90
            send = qrs.lateend+1
            #TODO test including all P wave info
            sstart, bstart = sstart-90, 0
#                if pw is not None:
#                    if sstart-pw.earlystart > 90:
#                        sstart, bstart = sstart-90, 0
#                    else:
#                        sstart = pw.earlystart
#                        bstart = 90-(qrs.earlystart-pw.earlystart)
            if tw is not None:
                if tw.lateend+1 - qrs.earlystart > 165:
                    send = qrs.earlystart + 165
                else:
                    send = tw.lateend+1
            sfr = sig[int(sstart):int(send)]-sig[int(sstart)]
            bsig[int(bstart):int(bstart+len(sfr))] = sfr
            ift['signal'] = bsig.tolist()
            rows.append(ift)
    df = df.append(rows, ignore_index=True)
    #RR differentiation.
    if len(df) > 1:
        rrdiff = np.diff(df['RR'])
        df['RRdb'] = np.concatenate(([0], rrdiff))
        df['RRda'] = np.concatenate((rrdiff, [rrdiff[-1]]))
    return df

if __name__ == "__main__":
    import sys
    import os.path

    DB = '/home/local/tomas.teijeiro/cinc_challenge17/training/'
    REFERENCE = OD([(r, k) for r, k in csv.reader(open(DB + 'REFERENCE-v4.csv'))])
    CLASSES = {'N':0, 'A':1, 'O':2, '~':3}

    beg, end = int(sys.argv[1]), int(sys.argv[2])
    for recname in list(REFERENCE)[beg:end]:
        print('Extracting features for record: {0}'.format(recname))
        outfile = '{0}{1}_{2}_feat.csv'.format(DB, recname,
                                                   CLASSES[REFERENCE[recname]])
        if os.path.isfile(outfile):
            print ('Already processed record, skipping...')
            continue
        rec = MIT.load_MIT_record(DB + recname)
        annots = MIT.read_annotations(DB + recname + '.nqrs')
        j=0
        while (j < len(annots) and annots[j].time <= 1
                       and (annots[j].code != MIT.ECGCodes.NOTE
                                         or annots[j].aux != "Inverted lead")):
            j += 1
        inverted = j <len(annots) and annots[j].aux == "Inverted lead"
        if inverted:
            print('Inverted lead')
            rec.signal[0] = -1*rec.signal[0]
        interp = interp2annots.ann2interp(rec, annots)
        dataframe = get_rnn_features(interp, rec.signal[0])
        #Feature saving.
        dataframe.to_csv(outfile, index=False)
