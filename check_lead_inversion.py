#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 18:58:54 2017

@author: tomas.teijeiro
"""


import construe.acquisition.obs_buffer as obs_buffer
import construe.inference.reasoning as reasoning
import construe.knowledge.observables as o
import construe.acquisition.record_acquisition as IN
import construe.knowledge.abstraction_patterns as ap
from construe.model.interpretation import Interpretation
from feat_utils import get_axis, kde_mode
from construe.inference.searching import ilen
import numpy as np
import pickle


def full_segmentation():
    """Obtains an interpretation of the full evidence in terms of P waves,
    QRS complexes and T waves."""
    #Trivial interpretation
    root = node = Interpretation()
    #The focus is initially set in the first observation
    try:
        root.focus.push(next(obs_buffer.get_observations()), None)
    except StopIteration:
        return root
    successors = {node:reasoning.firm_succ(node)}
    ########################
    ### Greedy searching ###
    ########################
    while True:
        try:
            node = next(successors[node])
            if node not in successors:
                successors[node] = reasoning.firm_succ(node)
        except StopIteration:
            #If the focus contains a top-level hypothesis, then there is no more
            #evidence to explain
            if isinstance(node.focus.top[0], o.Normal_Cycle):
                break
            else:
                #In other case, we perform a backtracking operation
                node = node.parent
        except KeyError:
            return root
    node.recover_all()
    return node

def get_wave_amplitude(qrs,n):
    """Obtains the amplitude of a given wave from a QRS complex"""
    waves = next(iter(qrs.shape.values())).waves
    return waves[n].amp if n < len(waves) else 0

def feature_vector(node, sig):
    """Obtains the feature vector for a given interpretation over a signal."""
    axis_list = [get_axis(qrs) for qrs in node.get_observations(o.QRS)]
    amplitude_list = [next(iter(t.amplitude.values()))
                                  for t in node.get_observations(o.TWave)] or 0
    basal_line = kde_mode(sig)
    energy_pos = np.sum(sig[np.where(sig>basal_line)]-basal_line)
    energy_neg = np.sum(sig[np.where(sig<basal_line)]-basal_line)
    energy_ratio = -energy_pos/energy_neg
    dispersion = -(max(sig)-basal_line)/(min(sig)-basal_line)
    std = np.std(sig)
    ext_values = [sig[t] for t in range(len(sig)) if sig[t]>basal_line+3*std
                                                   or sig[t]<basal_line-3*std]
    amplitude_wave_0 = np.median([get_wave_amplitude(qrs, 0)
                                      for qrs in node.get_observations(o.QRS)])
    amplitude_wave_1 = np.median([get_wave_amplitude(qrs, 1)
                                      for qrs in node.get_observations(o.QRS)])
    amplitude_wave_2 = np.median([get_wave_amplitude(qrs, 2)
                                      for qrs in node.get_observations(o.QRS)])
    siglen = float(len(sig))
    number_P = ilen(node.get_observations(o.PWave))/siglen
    number_QRS = ilen(node.get_observations(o.QRS))/siglen
    number_T = ilen(node.get_observations(o.TWave))/siglen
    vector = [np.median(axis_list), np.median(amplitude_list),
              (np.mean(sig)-np.median(sig))/siglen,
              (np.mean(sig)-np.median(sig))/np.ptp(sig),
              energy_ratio,basal_line, dispersion, np.mean(ext_values),
              amplitude_wave_0, amplitude_wave_1, amplitude_wave_2, number_P,
              number_QRS, number_T]
    return np.nan_to_num(vector)

def check_lead_inversion():
    """
    Checks if the signal is inverted, and if it is, changes the signal
    buffer accordingly. Returns a boolean confirming if the inversion took
    place.
    """
    prev_knowledge = ap.KNOWLEDGE
    #This test uses only segmentation knowledge
    ap.set_knowledge_base(ap.SEGMENTATION_KNOWLEDGE)
    node = full_segmentation()
    ap.set_knowledge_base(prev_knowledge)
    # clf = pickle.load(open('models/lg_fit.pkl','rb'))
    with open('models/lg_fit.pkl', 'rb') as f:
        ## python 2/3 compatibility :
        u = pickle._Unpickler(f)
        u.encoding = 'latin1'
        clf = u.load()
    features = feature_vector(node, IN.SIG.get_signal())
    return clf.predict_proba(np.array(features).reshape(1, -1))[0, 1]

if __name__ == "__main__":
    import time
    import argparse

    #Argument parsing
    parser = argparse.ArgumentParser(description=
               'Performs the P-wave, QRS and T-wave delineation of an ECG record.')
    parser.add_argument('-r', metavar='record', required=True,
                        help='Record to be processed')
    parser.add_argument('-a', metavar='ann', default='qrs',
                        help='Annotator used to set the initial evidence')
    args = parser.parse_args()
    #Searching settings
    TFACTOR = 1e20
    KFACTOR = 1
    #All the evidence is read before the interpretation
    IN.reset()
    IN.set_record(args.r, args.a)
    IN.set_duration(int(IN.get_record_length()/IN._STEP)*IN._STEP)
    IN.set_tfactor(TFACTOR)
    IN.start()
    while obs_buffer.get_status() == obs_buffer.Status.ACQUIRING:
        IN.get_more_evidence()
    prev_knowledge = ap.KNOWLEDGE
    ap.set_knowledge_base(ap.SEGMENTATION_KNOWLEDGE)
    print('Starting P-wave, QRS and T-wave delineation')
    t0 = time.time()
    node = full_segmentation()
    print('Finished in {0:.3f} seconds'.format(time.time()-t0))
    ap.set_knowledge_base(prev_knowledge)
    clf = pickle.load(open('lg_fit.pkl', 'rb'))
    features = np.array(feature_vector(node, IN.SIG.get_signal())).reshape(1,-1)
    invprob = clf.predict_proba(features)[0, 1]
    assert bool(clf.predict(features)) == bool(invprob > 0.5)
    print('Record {0} inversion probability: {1}'.format(args.r, invprob))
    if invprob > 0.5:
        print('Record {0} should be inverted.'.format(args.r))
