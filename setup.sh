#! /bin/bash
#
# file: setup.sh
#
# This bash script performs any setup necessary in order to test your
# entry.  It is run only once, before running any other code belonging
# to your entry.

set -e
set -o pipefail

#Additional python packages
pip install --user sortedcontainers-1.5.7.tar.gz
pip install --user --no-dependencies xgboost-0.6a2.tar.gz

# Example: compile a C module (viterbi_Schmidt) for Octave
#mkoctfile -mex viterbi_Springer.c

# Example: compile a C module for Matlab
#mex viterbi_Springer.c

cd construe/utils/signal_processing/dtw
gcc -O -shared -fPIC *.c -o dtw.so -I /usr/include/python2.7/
