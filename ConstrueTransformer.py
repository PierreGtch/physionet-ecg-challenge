import numpy as np
from sklearn.preprocessing import OneHotEncoder, RobustScaler
from sklearn.base import TransformerMixin

class ConstrueTransformer(TransformerMixin):
    def __init__(self, PREPROC_VARS, inputer_value=-10.0):
        self.n_categorical = len(PREPROC_VARS['categorical_feat'])
        self.inputer_value = inputer_value
        self.feature_index = {k: PREPROC_VARS[k] for k in ('RR', 'QRSd', 'PR','RRda')}
        self.n_rh = PREPROC_VARS['n_rh']
        self.n_morph = PREPROC_VARS['n_morhp']

        # a lot of scalers to handle nans
        self.scalers = []
        self.oh_encoder = OneHotEncoder(n_values=[self.n_rh, self.n_morph],
                                        categorical_features=PREPROC_VARS['oh_feat'],
                                        dtype='float32', sparse=False)

    def __add_features(self, X):
        return np.concatenate([X,
                               (X[:, self.feature_index['RR']] - 1200).reshape((-1, 1)),
                               (600 - X[:, self.feature_index['RR']]).reshape((-1, 1)),
                               (X[:, self.feature_index['QRSd']] - 110).reshape((-1, 1)),
                               (X[:, self.feature_index['PR']] - 210).reshape((-1, 1)),
                               (np.abs(X[:, self.feature_index['RRda']]) - 50).reshape((-1, 1))
        ], axis=1)

    def fit(self, X):
        Xcopy = X.copy()
        Xcopy = self.__add_features(Xcopy)
        self.scalers = []
        self.oh_encoder.fit(Xcopy[:, :self.n_categorical])
        for i, feat in enumerate(range(self.n_categorical, Xcopy.shape[1])):
            self.scalers.append(RobustScaler(quantile_range=(10.0, 90.0)))
            non_nan = self._which_non_nan(Xcopy[:, feat])
            if len(non_nan) == 0:
                self.scalers[i].fit(np.zeros((2, 1)))
            else:
                self.scalers[i].fit(Xcopy[non_nan, feat].reshape(-1, 1))
                self.scalers[i].center_ = np.mean(Xcopy[non_nan, feat])
                self.scalers[i].scale_ = np.median(np.abs(Xcopy[non_nan, feat] - self.scalers[i].center_))
        return self

    def _which_non_nan(self, feature):
        return np.where(np.logical_not(np.isnan(feature.flatten())))[0]

    def transform(self, X):
        Xcopy = X.copy()
        Xcopy = self.__add_features(Xcopy)
        if len(self.scalers) != (Xcopy.shape[1] - self.n_categorical):
            raise Exception('Call fit() method first')
        Xhead = self.oh_encoder.transform(Xcopy[:, :self.n_categorical])
        Xtail = Xcopy[:, self.n_categorical:]
        for i, feat in enumerate(range(self.n_categorical, Xcopy.shape[1])):
            non_nan = self._which_non_nan(Xcopy[:, feat])
            if len(non_nan) > 0:
                Xtail[non_nan, i] = self.scalers[i].transform(Xcopy[non_nan, feat].reshape(-1, 1)).flatten()

        # use X to avoid using the transformed features
        Xhead = np.concatenate([
            Xhead,
            (X[:, self.feature_index['RR']] >= 1200).reshape((-1, 1)).astype(float),
            (600 >= X[:, self.feature_index['RR']]).reshape((-1, 1)).astype(float),
            (X[:, self.feature_index['QRSd']] >= 110).reshape((-1, 1)).astype(float),
            (X[:, self.feature_index['PR']] >= 210).reshape((-1, 1)).astype(float),
            (np.abs(X[:, self.feature_index['RRda']]) <= 50).reshape((-1, 1)).astype(float)
        ], axis=1)
        # eliminate morph
        Xhead = np.concatenate([Xhead[:, :self.n_rh],
                                Xhead[:, (self.n_rh + self.n_morph):]
                                ], axis=1)
        Xcopy = np.concatenate([Xhead, Xtail], axis=1)
        Xcopy[np.isnan(Xcopy)] = self.inputer_value
        return Xcopy
