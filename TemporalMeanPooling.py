from keras.engine.topology import Layer, InputSpec
from keras import backend as K
import numpy as np

class TemporalMeanPooling(Layer):
    """
    Temporal Mean pooling that assumes that MASK VALUES ARE 0!!
    input shape: (nb_samples, nb_timesteps, nb_features)
    output shape: (nb_samples, nb_features)
    Adapted from:
    https://github.com/fchollet/keras/issues/2151
    """
    def __init__(self, **kwargs):
        super(TemporalMeanPooling, self).__init__(**kwargs)
        self.supports_masking = True
        self.input_spec = [InputSpec(ndim=3)]

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[2])

    def call(self, x, mask=None):
        # mask: (nb_samples, nb_timesteps)
        if mask is None:
            mask = K.mean(K.ones_like(x), axis=-1)
        # (nb_samples, np_features)
        x_sum = K.sum(x, axis=1)
        nb_non_masked = K.sum(K.cast(mask, K.floatx()), axis=-1, keepdims=True)
        nb_non_masked = K.cast(K.tile(nb_non_masked, (1, K.int_shape(x)[-1])), K.floatx())
        return x_sum / nb_non_masked

    def compute_mask(self, input, mask):
        return None

