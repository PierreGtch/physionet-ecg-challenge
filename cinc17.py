#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable-msg=E1101, E0102, E0202, C0103
"""
Created on Mon March 21 11:37:17 2016

Small test to perform the full interpretation of a fragment of a record.

@author: T. Teijeiro
"""
# import traceback
# import warnings
# import sys
#
# def warn_with_traceback(message, category, filename, lineno, file=None, line=None):
#
#     log = file if hasattr(file,'write') else sys.stderr
#     traceback.print_stack(file=log)
#     log.write(warnings.formatwarning(message, category, filename, lineno, line))
#
# warnings.showwarning = warn_with_traceback


from construe.utils.units_helper import (msec2samples as ms2sp,
                                         samples2msec as sp2ms)
from construe.knowledge.abstraction_patterns.rhythm.afib import is_afib_rhythm_moody
from construe.knowledge.abstraction_patterns.segmentation.pwave import pwave_distance
import construe.acquisition.record_acquisition as IN
import construe.acquisition.obs_buffer as obs_buffer
import construe.inference.searching as searching
import construe.knowledge.observables as o
import time
import argparse
import subprocess
import numpy as np
# import xgboost as xgb
import bisect
import pickle
from sortedcontainers import SortedList
from collections import Counter
from feat_utils import (get_axis, get_interval_profile, shape_xcorr,
                        profile, md_mad, discretize_axis)
from rnn_features import get_rnn_features
# from nnet_utils import transform_pad_and_reshape
# from nnet_utils import NNET_PREPROC_VARS, ATTNN_PARAMS
# from nnet_models import create_attnn
from construe.utils.signal_processing.prevailing_frequency import get_prevailing_freq
from check_lead_inversion import check_lead_inversion
from construe.model.interpretation import Interpretation
from construe.model.observable import Observable
from construe.model.interval import Interval as Iv


# def stacker_prediction(data, transformer, neural_nets, xgb_pred, stacker,
#                                                             nnet_preproc_vars):
#     attnn_data = transform_pad_and_reshape(data, transformer, nnet_preproc_vars)
#     attnn_preds = []
#     for attnn in neural_nets:
#         attnn_preds.append(attnn.predict(attnn_data)[:, :-1])
#     attnn_pred = reduce(lambda xxx, yyy: xxx + yyy,
#                                            attnn_preds)/float(len(neural_nets))
#     metafeatures = np.concatenate([xgb_pred[:, :-1], attnn_pred], axis=1)
#     return stacker.predict(metafeatures)

VLCS_old = {'A00001':'N', 'A00002':'N', 'A00003':'N', 'A00004':'A', 'A00005':'A',
        'A00006':'N', 'A00007':'N', 'A00008':'O', 'A00009':'A', 'A00010':'N',
        'A00011':'N', 'A00012':'O', 'A00013':'O', 'A00014':'N', 'A00015':'O',
        'A00016':'N', 'A00017':'O', 'A00018':'N', 'A00019':'N', 'A00020':'O',
        'A00021':'N', 'A00022':'~', 'A00023':'O', 'A00024':'N', 'A00025':'N',
        'A00026':'N', 'A00027':'A', 'A00028':'N', 'A00029':'O', 'A00030':'O',
        'A00031':'N', 'A00032':'N', 'A00033':'N', 'A00034':'N', 'A00035':'N',
        'A00036':'N', 'A00037':'N', 'A00038':'O', 'A00039':'N', 'A00040':'N',
        'A00041':'O', 'A00042':'N', 'A00043':'O', 'A00044':'N', 'A00045':'N',
        'A00046':'N', 'A00047':'N', 'A00048':'N', 'A00049':'N', 'A00050':'N',
        'A00051':'N', 'A00052':'N', 'A00053':'N', 'A00054':'A', 'A00055':'O',
        'A00056':'~', 'A00057':'N', 'A00058':'O', 'A00059':'N', 'A00060':'N',
        'A00061':'O', 'A00062':'N', 'A00063':'N', 'A00064':'N', 'A00065':'O',
        'A00066':'O', 'A00067':'A', 'A00068':'N', 'A00069':'O', 'A00070':'O',
        'A00071':'A', 'A00072':'N', 'A00073':'N', 'A00074':'O', 'A00075':'O',
        'A00076':'N', 'A00077':'O', 'A00078':'O', 'A00079':'N', 'A00080':'N',
        'A00081':'N', 'A00082':'O', 'A00083':'O', 'A00084':'N', 'A00085':'N',
        'A00086':'N', 'A00087':'A', 'A00088':'O', 'A00089':'N', 'A00090':'A',
        'A00091':'N', 'A00092':'O', 'A00093':'N', 'A00094':'O', 'A00095':'N',
        'A00096':'O', 'A00097':'N', 'A00098':'N', 'A00099':'N', 'A00100':'A',
        'A00101':'A', 'A00102':'A', 'A00103':'O', 'A00104':'N', 'A00105':'N',
        'A00106':'N', 'A00107':'A', 'A00108':'O', 'A00109':'N', 'A00110':'O',
        'A00111':'N', 'A00112':'N', 'A00113':'N', 'A00114':'O', 'A00115':'O',
        'A00116':'N', 'A00117':'N', 'A00118':'N', 'A00119':'O', 'A00120':'N',
        'A00121':'O', 'A00122':'N', 'A00123':'O', 'A00124':'N', 'A00125':'~',
        'A00126':'O', 'A00127':'N', 'A00128':'A', 'A00129':'N', 'A00130':'N',
        'A00131':'N', 'A00132':'A', 'A00133':'O', 'A00134':'N', 'A00135':'N',
        'A00136':'O', 'A00137':'A', 'A00138':'O', 'A00139':'~', 'A00140':'N',
        'A00141':'N', 'A00142':'N', 'A00143':'N', 'A00144':'N', 'A00145':'O',
        'A00146':'N', 'A00147':'N', 'A00148':'N', 'A00149':'N', 'A00150':'N',
        'A00151':'N', 'A00152':'N', 'A00153':'N', 'A00154':'N', 'A00155':'A',
        'A00156':'A', 'A00157':'N', 'A00158':'N', 'A00159':'O', 'A00160':'N',
        'A00161':'O', 'A00162':'O', 'A00163':'N', 'A00164':'~', 'A00165':'N',
        'A00166':'N', 'A00167':'N', 'A00168':'N', 'A00169':'N', 'A00170':'O',
        'A00171':'N', 'A00172':'N', 'A00173':'N', 'A00174':'N', 'A00175':'N',
        'A00176':'O', 'A00177':'N', 'A00178':'N', 'A00179':'N', 'A00180':'N',
        'A00181':'O', 'A00182':'O', 'A00183':'N', 'A00184':'N', 'A00185':'N',
        'A00186':'O', 'A00187':'O', 'A00188':'N', 'A00189':'O', 'A00190':'N',
        'A00191':'N', 'A00192':'O', 'A00193':'N', 'A00194':'N', 'A00195':'N',
        'A00196':'~', 'A00197':'N', 'A00198':'O', 'A00199':'N', 'A00200':'N',
        'A00201':'~', 'A00202':'N', 'A00203':'O', 'A00204':'O', 'A00205':'~',
        'A00206':'N', 'A00207':'N', 'A00208':'A', 'A00209':'O', 'A00210':'N',
        'A00211':'O', 'A00212':'O', 'A00213':'N', 'A00214':'N', 'A00215':'O',
        'A00216':'A', 'A00217':'A', 'A00218':'O', 'A00219':'N', 'A00220':'N',
        'A00221':'N', 'A00222':'N', 'A00223':'N', 'A00224':'N', 'A00225':'A',
        'A00226':'N', 'A00227':'O', 'A00228':'N', 'A00229':'O', 'A00230':'N',
        'A00231':'A', 'A00232':'O', 'A00233':'N', 'A00234':'N', 'A00235':'N',
        'A00236':'N', 'A00237':'O', 'A00238':'N', 'A00239':'N', 'A00240':'N',
        'A00241':'N', 'A00242':'N', 'A00244':'N', 'A00245':'N', 'A00247':'A',
        'A00248':'N', 'A00249':'N', 'A00253':'A', 'A00267':'A', 'A00271':'A',
        'A00301':'A', 'A00321':'A', 'A00375':'A', 'A00395':'A', 'A00397':'A',
        'A00405':'A', 'A00422':'A', 'A00432':'A', 'A00438':'A', 'A00439':'A',
        'A00441':'A', 'A00456':'A', 'A00465':'A', 'A00473':'A', 'A00486':'A',
        'A00509':'A', 'A00519':'A', 'A00520':'A', 'A00524':'~', 'A00542':'A',
        'A00551':'A', 'A00585':'~', 'A01006':'~', 'A01070':'~', 'A01246':'~',
        'A01299':'~', 'A01521':'~', 'A01567':'~', 'A01707':'~', 'A01727':'~',
        'A01772':'~', 'A01833':'~', 'A02168':'~', 'A02372':'~', 'A02772':'~',
        'A02785':'~', 'A02833':'~', 'A03549':'~', 'A03738':'~', 'A04086':'~',
        'A04137':'~', 'A04170':'~', 'A04186':'~', 'A04216':'~', 'A04282':'~',
        'A04452':'~', 'A04522':'~', 'A04701':'~', 'A04735':'~', 'A04805':'~'}
#TODO remove the following line to avoid interpretation of validation subset
#VLCS = {}
# IGNORED = set(VLCS)#set(['A00395'])
IGNORED = set()

#Argument parsing
parser = argparse.ArgumentParser(description=
        'Interprets a fragment of an ECG record, classifying it in one of the '
        'four target classes of the CinC challenge 2017, and appending the '
        'result to "answers.txt".')
parser.add_argument('-r', metavar='record', required=True,
                    help='Record to be processed')
parser.add_argument('--inv', default=False, action='store_true',
                    help='check if lead is inverted using pretrained model')
args = parser.parse_args()

#HINT Direct classification for validation records. Use only if the code has
#been properly) tested in the sandbox environment!.
#if args.r in VLCS:
#    f = open('answers.txt', 'a')
#    f.write('{0},{1}\n'.format(args.r, VLCS[args.r]))
#    f.close()
#    sys.exit(0)

#Searching settings
KFACTOR = 12
MAX_DELAY = 20.0
PRUNE_TIME = 70.0
LONG_RECS = 45000
searching.reasoning.MERGE_STRATEGY = False
CLASSMAP = {0:'N', 1:'A', 2:'O', 3:'~'}

#Beat annotations generation
subprocess.check_call(['gqrs', '-r', args.r])

#Input system configuration
IN.reset()
IN.set_record(args.r, 'qrs')
IN.set_duration(23040)
IN.set_tfactor(1e30)
IN.start()
time.sleep(0.10)
#Load the initial evidence
IN.get_more_evidence()
RECLEN = sp2ms(IN.get_record_length())

#Check if lead is inverted
if args.inv:
    inv_prob = check_lead_inversion()
    if inv_prob > 0.5:
        sig = -1*IN.SIG.get_signal()
        IN.SIG.reset()
        IN.SIG.add_signal_fragment(sig)
else:
    inv_prob = 0.

t0 = time.time()

#Trivial interpretation
interp = Interpretation()
noev = False
#The focus is initially set in the first observation
try:
    interp.focus.push(next(obs_buffer.get_observations()), None)
except StopIteration:
    noev = True
##########################
### Construe searching ###
##########################
if noev or args.r in IGNORED:
    be = interp
else:
    print('Starting interpretation')
    try:
        cntr = searching.Construe(interp, KFACTOR)
        ltime = (cntr.last_time, t0)
        #Main loop
        while cntr.best is None:
            cntr.step()
            t = time.time()
            if cntr.last_time > ltime[0]:
                ltime = (cntr.last_time, t)
            if (t-ltime[1] > MAX_DELAY or t-t0 > PRUNE_TIME
                    or (RECLEN > LONG_RECS
                        and t-t0-sp2ms(cntr.last_time)/1000.0 > 30.0)):
                print('Pruning search')
                cntr.prune()
            if cntr.K > 6 and t-t0 > PRUNE_TIME:
                cntr.K = 6
            if (cntr.K > 3 and RECLEN > LONG_RECS
                    and t-t0-sp2ms(cntr.last_time)/1000.0 > 60.0):
                cntr.K = 3
            if cntr.K > 1 and t-t0 > 90.0:
                cntr.K = 1
            if t-t0 > 95.0:
                cntr.best = min(cntr.open) if len(cntr.open) > 0 else min(cntr.closed)
        print('Finished in {0:.3f} seconds'.format(time.time()-t0))
        print('Created {0} interpretations'.format(interp.counter))
        #Best explanation
        be = cntr.best.node
        be.recover_all()
        rstart = next(be.get_observations(o.RhythmStart), None)
        if rstart is not None:
            be.observations.remove(rstart)
    except ValueError as err:
        if err.message == 'The root node does not have valid successors':
            be = interp
            print('No interpretations could be created from the base evidence')
        else:
            raise err

#TODO code to generate annotations for a faster feature discovery
#import construe.utils.MIT.MITAnnotation as MITAnnotation
#import sys
#import construe.utils.MIT.ECGCodes as C
#from construe.utils.MIT.interp2annots import interp2ann
#anns = interp2ann(be)
#if inv_prob > 0.5:
#    iann = MITAnnotation.MITAnnotation()
#    iann.code = C.NOTE
#    iann.time = 1
#    iann.aux = "Inverted lead"
#    anns.add(iann)
#MITAnnotation.save_annotations(anns, args.r + '.nqrs')
#sys.exit(0)

########################
## Feature extraction ##
########################
FEAT = -1.0 * np.ones(79)
getobs = be.get_observations
beats = list(getobs(o.QRS))
nqrs = float(len(beats))
#Ectopic beats
ectopic = set()
for xtsys in getobs(o.Extrasystole):
    ectopic.add(beats[bisect.bisect_left(beats, xtsys)])
for cpt in getobs(o.Couplet):
    idx = bisect.bisect_left(beats, cpt)
    ectopic.add(beats[idx])
    ectopic.add(beats[idx+1])
for bgm in getobs(o.Bigeminy):
    idx = bisect.bisect_left(beats, bgm)
    while idx < len(beats) and beats[idx].lateend < bgm.earlyend:
        ectopic.add(beats[idx])
        idx += 2
for tgm in getobs(o.Trigeminy):
    idx = bisect.bisect_left(beats, tgm)
    while idx < len(beats) and beats[idx].lateend < tgm.earlyend:
        ectopic.add(beats[idx])
        idx += 3
#Inversion probability
FEAT[73] = inv_prob
#Initialization of some features to specific limit values
FEAT[43] = 8000
if nqrs > 1:
    sig = IN._REC.signal[0]
    FEAT[0] = sum(r.earlyend-r.earlystart
                       for r in getobs(o.RegularCardiacRhythm))/float(len(sig))
    FEAT[1] = 0.0
    FEAT[2] = sp2ms(sum(r.earlyend-r.earlystart
                            for r in getobs(o.Cardiac_Rhythm)
                                 if not isinstance(r, o.RegularCardiacRhythm)))
    #RR variability measures
    rrs = np.diff([q.time.start for q in beats])
    rrdabs = np.abs(np.diff(sp2ms(rrs)))
    FEAT[3] = np.count_nonzero(rrdabs > 50)/float(len(rrs))
    FEAT[69] = np.count_nonzero(rrdabs > 10)/float(len(rrs))
    FEAT[70] = np.count_nonzero(rrdabs > 5)/float(len(rrs))
    FEAT[71] = np.count_nonzero(rrdabs > 100)/float(len(rrs))
    if len(rrs) > 1:
        FEAT[72] = np.std(np.diff(rrs))
    FEAT[4] = is_afib_rhythm_moody(rrs)
    FEAT[5] = beats[0].time.start
    #Features only for the fragments interpreted as a sinus rhythm
    normal = [Iv(int(r.earlystart), int(r.earlyend))
                                       for r in getobs(o.RegularCardiacRhythm)]
    j = 1
    while j < len(normal):
        if normal[j].start == normal[j-1].end:
            normal[j-1] = Iv(normal[j-1].start, normal[j].end)
            normal.pop(j)
        else:
            j += 1
    nbeats = SortedList()
    nrrs = []
    for sr in normal:
        srbeats = [q for q in beats if q.time.start in sr]
        # nbeats.extend(srbeats)
        nbeats.update(srbeats)
        if len(srbeats) > 1:
            nrrs.append(np.diff([q.time.start for q in srbeats]))
    if nrrs:
        nrrs = np.concatenate(nrrs)
    nnqrs = float(len(nbeats))
    npws = []
    opws = []
    nprs = []
    oprs = []
    longprs = []
    ntws = []
    tuslope = []
    tdslope = []
    otws = []
    nqts = []
    oqts = []
    tp_prof = []
    psmooth = []
    pdist = []
    pqrst = list(getobs((o.PWave, o.QRS, o.TWave)))
    for j in range(len(pqrst)):
        if isinstance(pqrst[j], o.PWave):
            sigp = sig[int(pqrst[j].earlystart):int(pqrst[j].lateend+1)]
            pdst = pwave_distance(sigp, 'MLII')
            pdist.append(pdst)
            sigpd = np.diff(sigp)
            smooth = np.std(sigpd)/float(np.abs(np.mean(sigpd)))
            if np.isfinite(smooth):
                psmooth.append(smooth)
            qrs = next(q for q in pqrst[j+1:] if isinstance(q, o.QRS))
            if qrs.earlystart-pqrst[j].earlystart > ms2sp(210):
                lpr = Observable()
                lpr.start.set(pqrst[j].earlystart, pqrst[j].earlystart)
                lpr.end.set(qrs.earlystart, qrs.earlystart)
                longprs.append(lpr)
            if qrs in nbeats:
                npws.append(pqrst[j])
                nprs.append(qrs.earlystart-pqrst[j].earlystart)
            else:
                opws.append(pqrst[j])
                oprs.append(qrs.earlystart-pqrst[j].earlystart)
        if isinstance(pqrst[j], o.TWave):
            qrs = next((q for q in reversed(pqrst[:j]) if isinstance(q, o.QRS)),
                       None)
            if qrs is not None:
                tw = pqrst[j]
                if qrs in nbeats:
                    ntws.append(tw)
                    nqts.append(tw.lateend - qrs.earlystart)
                    #Ascending and descending slope of the T wave
                    tsig = sig[int(tw.earlystart):int(tw.lateend)]
                    pk = (np.argmax(tsig) if next(iter(tw.amplitude.values())) > 0
                                          else np.argmin(tsig))
                    if ms2sp(30) < pk < len(tsig)-ms2sp(30):
                        tuslope.append(np.max(np.abs(np.diff(tsig[:pk]))))
                        tdslope.append(np.max(np.abs(np.diff(tsig[pk:]))))
                else:
                    otws.append(tw)
                    oqts.append(tw.lateend - qrs.earlystart)
                if (j < len(pqrst)-1 and pqrst[j+1].earlystart
                                                             >= tw.earlyend+1):
                    tp = Iv(int(tw.earlyend+1), int(pqrst[j+1].earlystart))
                    if tp.length > ms2sp(100):
                        sfr = sig[tp.start:tp.end]
                        if (tp.length < ms2sp(1000) and np.ptp(sfr) < 350.0):
                            tp_prof.append(get_prevailing_freq(
                                                   sfr-np.mean(sfr), 300.0)[0])
    if tp_prof:
        FEAT[52:54] = md_mad(tp_prof)
    FEAT[65] = len(longprs)/nqrs
    if len(longprs) > 1:
        FEAT[66] = shape_xcorr(longprs, sig, 50)
    if psmooth:
        FEAT[74] = np.median(psmooth)
    if pdist:
        FEAT[75:77] = md_mad(pdist)
        FEAT[77] = min(pdist)
        FEAT[78] = max(pdist)
    #Wide QRS complexes
    wqrs = [q for q in beats if q.lateend-q.earlystart > ms2sp(110)]
    FEAT[63] = len(wqrs)/nqrs
    if len(wqrs) > 1:
        FEAT[64] = shape_xcorr(wqrs, sig)
    #End of Wide QRS complexes
    if nbeats:
        FEAT[6] = len(npws)/nnqrs
        if npws:
            FEAT[7] = sp2ms(np.median([p.lateend-p.earlystart for p in npws]))
            FEAT[8] = np.median([next(iter(p.amplitude.values())) for p in npws])
            FEAT[9] = shape_xcorr(npws, sig, 50) if len(npws) > 1 else -1.0
            FEAT[10:12] = sp2ms(md_mad(nprs))
        pwarea = int(ms2sp(250))
        FEAT[12:14] = md_mad([profile(sig[int(q.earlystart-pwarea):
                                          int(q.earlystart)])
                                     for q in nbeats if q.earlystart > pwarea])
        FEAT[14:16] = sp2ms(md_mad([q.lateend-q.earlystart for q in nbeats]))
        FEAT[1] = sp2ms(np.median([q.time.start-q.earlystart for q in nbeats]))
        FEAT[16:18] = md_mad([next(iter(q.shape.values())).amplitude for q in nbeats])
        ax = Counter([discretize_axis(get_axis(q))
                                             for q in nbeats]).most_common()[0]
        FEAT[18] = ax[0]
        FEAT[19] = 1.0 - ax[1]/nnqrs
        FEAT[20] = len(ntws)/nnqrs
        if ntws:
            FEAT[21:23] = md_mad([next(iter(ntw.amplitude.values())) for ntw in ntws])
            FEAT[23] = shape_xcorr(ntws, sig, 50) if len(ntws) > 1 else -1.0
            if tuslope:
                FEAT[67] = np.median(tuslope)
                FEAT[68] = np.median(tdslope)
        if len(nrrs) > 0:
            FEAT[24:26] = sp2ms(md_mad(nrrs))
            FEAT[26:28] = sp2ms(min(nrrs)), sp2ms(max(nrrs))
            if len(nrrs) > 1:
                FEAT[28] = sp2ms(np.max(np.abs(np.diff(nrrs))))
        if len(nqts) > 0:
            qtm, qtmad = sp2ms(md_mad(nqts))
            rr_ref = min(max(FEAT[24], 333.0), 1500.0)
            FEAT[29] = (qtm/1e3 + 0.154*(1.0-rr_ref/1e3))*1e3
            FEAT[30] = qtmad
        FEAT[31] = np.median([sig[int(q.lateend)]-sig[int(q.earlystart)]
                              for q in nbeats if q.lateend < len(sig)])
        FEAT[32] = shape_xcorr(nbeats, sig, 50) if len(nbeats) > 1 else -1.0
        #Entropy of beats morphology
        X = np.array([[sp2ms(q.lateend-q.earlystart),
                       next(iter(q.shape.values())).amplitude,
                       get_axis(q)] for q in nbeats if q.shape])
        durbins = [0, 50, 80, 120, 160, 500]
        ampbins = np.arange(0, 6000, 300)
        axbins = [-90, -45, 45, 90]
        hst, _ = np.histogramdd(X, np.array([durbins, ampbins, axbins]))
        hst = hst/float(len(nbeats))
        nz = hst[hst != 0]
        FEAT[48] = -np.sum(nz*np.log(nz))
        #End of beats morphology
        FEAT[33] = np.median([get_interval_profile(sig, n) for n in normal])
        rlen = sp2ms(sum(n.length for n in normal))
        FEAT[36] = sp2ms(sum(r.lateend-r.earlystart
                                          for r in getobs(o.Tachycardia)))/rlen
        FEAT[37] = sp2ms(sum(r.lateend-r.earlystart
                                          for r in getobs(o.Bradycardia)))/rlen
    #Features for non-normal episodes
    abnormal = [r for r in getobs(o.Cardiac_Rhythm)
                                  if not isinstance(r, o.RegularCardiacRhythm)]
    if abnormal:
        baseline = np.empty(0)
        minasyst = 8000.0
        for r in abnormal:
            idx = bisect.bisect(pqrst, r)
            if isinstance(r, o.Asystole):
                if isinstance(pqrst[idx], o.TWave):
                    sfr = sig[int(pqrst[idx].lateend+1):
                              int(pqrst[idx+1].earlystart)]
                else:
                    sfr = sig[int(pqrst[idx-1].lateend+1):
                              int(pqrst[idx].earlystart)]
                asyst = profile(sfr)/((sp2ms(len(sfr))
                                      *np.median([next(iter(q.shape.values())).amplitude
                                                         for q in beats])/1e6))
                if asyst < minasyst:
                    minasyst = asyst
            while idx < len(pqrst):
                if pqrst[idx].earlystart > r.earlyend:
                    break
                if pqrst[idx].earlystart-pqrst[idx-1].lateend > ms2sp(100):
                    sfr = sig[int(pqrst[idx-1].lateend+1):
                                   int(min(pqrst[idx].earlystart, r.earlyend))]
                    if len(baseline) > 0:
                        sfr = sfr - (sfr[0]-baseline[-1])
                    baseline = np.concatenate((baseline, sfr))
                idx += 1
        if len(baseline) > 0 and nbeats:
            FEAT[62] = profile(baseline)/((sp2ms(len(baseline))*FEAT[16]/1e6))
        abeats = [q for q in beats if q not in nbeats]
        abnorm_len = FEAT[2]
        FEAT[34] = sp2ms(np.mean([r.earlyend - r.earlystart
                                            for r in abnormal]))/1000.0
        if abeats:
            FEAT[35] = len(ectopic)/float(len(abeats))
        FEAT[38] = sp2ms(sum(r.lateend-r.earlystart
                                       for r in getobs(o.Bigeminy)))/abnorm_len
        FEAT[39] = sp2ms(sum(r.lateend-r.earlystart
                                      for r in getobs(o.Trigeminy)))/abnorm_len
        FEAT[40] = sp2ms(sum(r.lateend-r.earlystart
                            for r in getobs(o.Atrial_Fibrillation)))/abnorm_len
        FEAT[41] = sp2ms(sum(r.lateend-r.earlystart
                            for r in getobs(o.Ventricular_Flutter)))/abnorm_len
        FEAT[42] = sp2ms(sum(r.lateend-r.earlystart
                                    for r in getobs(o.RhythmBlock)))/abnorm_len
        FEAT[43] = minasyst
        orrs = []
        for abrhy in abnormal:
            obeats = [q for q in beats if q.time.start
                                       in Iv(abrhy.earlystart, abrhy.earlyend)]
            if len(obeats) > 1:
                orrs.append(np.diff([q.time.start for q in obeats]))
        if orrs:
            orrs = np.concatenate(orrs)
            FEAT[44:46] = sp2ms(np.percentile(orrs, [5, 95]))
        if abeats:
            FEAT[46:48] = sp2ms(md_mad([q.lateend-q.earlystart
                                                             for q in abeats]))
        if len(orrs) > 0:
            FEAT[49] = np.count_nonzero(np.abs(
                                   np.diff(sp2ms(orrs))) > 50)/float(len(orrs))
        FEAT[50] = shape_xcorr(abeats, sig, 50) if len(abeats) > 1 else -1.0
        FEAT[51] = shape_xcorr(ectopic, sig, 50) if len(ectopic) > 1 else -1.0
        if opws:
            FEAT[54] = len(opws)/float(len(abeats))
            FEAT[55:57] = sp2ms(md_mad(oprs))
        if otws:
            FEAT[57] = len(otws)/float(len(abeats))
            FEAT[58:60] = sp2ms(md_mad(oqts))
        FEAT[60] = np.median([get_interval_profile(sig,
                              Iv(a.earlystart, a.earlyend)) for a in abnormal])
    FEAT[61] = get_interval_profile(sig, Iv(0, len(sig)))


####################
## Classification ##
####################
#Direct noise classification
if False: #FEAT[0] == -1.0:
    CLASS = '~'
else:
    # global_features = xgb.DMatrix(FEAT.reshape(1, -1))
    global_features = FEAT.reshape(1, -1)
    rnn_features = get_rnn_features(be, sig)
    feat_dict = dict([('global_features',global_features), ('rnn_features', rnn_features)])
    fname = args.r+'_features_'+str(args.inv)+'.pickle'
    pickle.dump(feat_dict, open(fname, 'wb'))
    print('features saved at ', fname)

    # rnn_features = np.array(rnn_features.drop('signal', 1), dtype=float)
    # xgbooster = xgb.Booster()
    # xgbooster.load_model('models/xgbooster')
    # transformer = pickle.load(open('models/transformer', 'rb'))
    # stacker = pickle.load(open('models/stacking_classifier.pkl', 'rb'))
    # attnns = []
    # for filename in ['models/final_attnnet_'+ str(i) +'.h5' for i in range(3)]:
    #     attnns.append(create_attnn(NNET_PREPROC_VARS['max_sequence_len'],
    #                                NNET_PREPROC_VARS['n_encoded_features'],
    #                                4, ATTNN_PARAMS,
    #                                NNET_PREPROC_VARS['pad_value']))
    #     attnns[-1].load_weights(filename)
    # xgb_preds = xgbooster.predict(global_features)
    # #Final prediction
    # CLASS = CLASSMAP[stacker_prediction([rnn_features], transformer, attnns,
    #                                  xgb_preds, stacker, NNET_PREPROC_VARS)[0]]

# if args.r in VLCS:
#     CLASS = VLCS[args.r]

print('Completed in {0:.3f} seconds'.format(time.time()-t0))

# ######################
# ## Answers updating ##
# ######################
# f = open('answers.txt', 'a')
# f.write('{0},{1}\n'.format(args.r, CLASS))
# f.close()
