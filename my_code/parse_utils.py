import os
import pickle
from scipy.io import loadmat

# from tqdm import tqdm_notebook as tqdm



RECORDS_SPLITTING_FILE = 'records_splitting.pickle'
def get_records_splitting(path=RECORDS_SPLITTING_FILE):
    return pickle.load(open(RECORDS_SPLITTING_FILE, 'rb'))


def numpy_from_mat(path):
    return loadmat(path)['val'].reshape(-1)

def rec_from_RECORDS_file(RECORDS_path, read_rec=numpy_from_mat):
    """ returns dict of rec_name->recording
    """
    data_dir = os.path.dirname(RECORDS_path)
    recordings = {}
    with open(RECORDS_path, "r") as f:
        for rec_name in f:
            rec_name = rec_name.strip()
            recordings[rec_name] = read_rec(os.path.join(data_dir, rec_name+'.mat'))
    return recordings


_LABELS_LIST = ['N','A','O','~']
_LABELS_TO_ID_DICT = {l:i for i,l in enumerate(_LABELS_LIST)}
def label_to_id(label):
    return _LABELS_TO_ID_DICT[label]
def id_to_label(idx):
    return _LABELS_LIST[idx]


def parse_REFERENCE_file(REFERENCE_path):
    """ return list of paisrs (rec_name, label)
    """
    out = []
    with open(REFERENCE_path, "r") as f:
        for l in f:
            rec_name, label = l.strip().split(',')
            out.append((rec_name, label))
    return out

def get_features(REFERENCE_path, data_dir, suffix='_features_False.pickle'):
    labels = dict(parse_REFERENCE_file(REFERENCE_path))
    n = len(suffix)
    out = {}
    for direct, _, files in os.walk(data_dir):
        for file in files:
            if file.endswith(suffix):
                name = file[:-n]
                l = labels[name]
                feat = pickle.load(open(os.path.join(direct, file), 'rb'))
                if name in out:
                    raise KeyError(name)
                out[name] = (feat['global_features'], feat['rnn_features'], label_to_id(l))
    return out


if __name__ == "__main__":
    from time import time

    refpath = '../training2017/REFERENCE.csv'
    datadir = '../training2017_results/'

    t0 = time()
    features = get_features(refpath, datadir, )
    t1 = time()

    print(len(features), ' files found')
    print('in {}s'.format(t1-t0))
