from sklearn.metrics import f1_score
import numpy as np
from parse_utils import id_to_label, label_to_id


def torch_ecg_f1_score(y_pred, labels):
    return ecg_f1_score(y_pred.cpu().detach().numpy(), labels.cpu().detach().numpy())

def ecg_f1_score(y_pred, labels):
    '''returns mean of F1 score for N, A, and O classes as explained in paper
    y_pred of shape [batch_size, num_classes]
    labels of shape [batch_size, 1]
    '''
    assert y_pred.shape[1] == 4
    labels = np.array(labels).reshape(-1)
    assert len(labels)==y_pred.shape[0]

    averaged_classes = [label_to_id(a) for a in ['N','A','O']]

    y_pred_pred = np.argmax(y_pred, axis=-1)

    return f1_score(labels, y_pred_pred, labels=averaged_classes, average='macro')




if __name__ == "__main__":
    ##parse_utils.py : _LABELS_LIST = ['N','A','O','~']
    llabels = [0,1,2, 3,3,3, 0,1,2]
    yy_pred = np.array([
        [1,0,0,0],
        [0,1,0,0],
        [0,0,1,0],

        [1,0,0,0],
        [0,1,0,0],
        [0,0,1,0],

        [0,0,0,1],
        [0,0,0,1],
        [0,0,0,1],
    ])
    ## here precision and recall are 0.5 for class A,N and O
    ## but 0.0 for class '~'
    print(ecg_f1_score(yy_pred, llabels))
    assert ecg_f1_score(yy_pred, llabels) == .5
